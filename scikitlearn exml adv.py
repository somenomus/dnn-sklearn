#use of Estimator API

from sklearn.datasets import load_wine
wine_data = load_wine()
type(wine_data)
wine_data.keys()
print(wine_data.DESCR)

feat_data = wine_data['data']
labels = wine_data['target']
from sklearn.model_selection import train_test_split

X_train, X_test, y_train, y_test = train_test_split(feat_data,
                                                    labels,
                                                    test_size=0.3,
                                                   random_state=101)
from sklearn.preprocessing import MinMaxScaler
scaler = MinMaxScaler()
scaled_x_train = scaler.fit_transform(X_train)
scaled_x_test = scaler.transform(X_test)

''' this is using ESTIMATOR API. LATER, will be KERAS. doing samein different API using. Comment one
and try another one.

import tensorflow as tf
from tensorflow import estimator 

print(X_train.shape)

feat_cols = [tf.feature_column.numeric_column("x", shape=[13])]

deep_model = estimator.DNNClassifier(hidden_units=[13,13,13],
                            feature_columns=feat_cols,
                            n_classes=3,
                            optimizer=tf.train.GradientDescentOptimizer(learning_rate=0.01) )

input_fn = estimator.inputs.numpy_input_fn(x={'x':scaled_x_train},y=y_train,shuffle=True,batch_size=10,num_epochs=5)

deep_model.train(input_fn=input_fn,steps=500)

#evaluation
input_fn_eval = estimator.inputs.numpy_input_fn(x={'x':scaled_x_test},shuffle=False)
preds = list(deep_model.predict(input_fn=input_fn_eval))
predictions = [p['class_ids'][0] for p in preds]
from sklearn.metrics import confusion_matrix,classification_report
print(classification_report(y_test,predictions))
'''


''' KERAS'''

import tensorflow as tf
from tensorflow.contrib.keras import models
dnn_keras_model = models.Sequential()
from tensorflow.contrib.keras import layers

dnn_keras_model.add(layers.Dense(units=13,input_dim=13,activation='relu'))

dnn_keras_model.add(layers.Dense(units=13,activation='relu'))
dnn_keras_model.add(layers.Dense(units=13,activation='relu'))
dnn_keras_model.add(layers.Dense(units=3,activation='softmax'))


from tensorflow.contrib.keras import losses,optimizers,metrics,activations

losses.sparse_categorical_crossentropy

#configing keras model
dnn_keras_model.compile(optimizer='adam',
              loss='sparse_categorical_crossentropy',   
              metrics=['accuracy'])
#if data is one hot encoded, can use loss = categorical_crossentropy

dnn_keras_model.fit(scaled_x_train,y_train,epochs=50)

predictions = dnn_keras_model.predict_classes(scaled_x_test)

print(classification_report(predictions,y_test))





















