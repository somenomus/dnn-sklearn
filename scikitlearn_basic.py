import numpy as np
from sklearn.preprocessing import MinMaxScaler

data = np.random.randint(0,100,(10,2))
data
scalar = MinMaxScaler()
type(scalar)
scalar.fit(data)

scalar.transform(data)

import pandas as pd
mydata = np.random.randint(0,101,(50,4))
mydata
df = pd.DataFrame(data=mydata, columns = ['f1', 'f2','f3','label'])
df
#split data
X = df[['f1','f2','f3']]
Y = df['label']

from sklearn.model_selection import train_test_split
X_train,X_test, y_train, y_test = train_test_split(X, Y, test_size=0.33, random_state=42) #random state for repitability.
X_train.shape
X_test.shape




#NOW<NEEW
#class operastions for manual Neural network

class Operation():
    def __init__(self, input_node=[]):
        self.input_node = input_node
        self.output_node = []
        
        for node in input_node:
            node.output_node.append(self)
            _default_graph.operations(self)
            
    def compute(self):
        pass
    
class add(Operation):
    def __init__(self,x,y):
        super().__init__([x,y])
        
    def compute(self,x_var,y_var):
        self.input = [x_var,y_var]
        return x_var + y_var
    

class multiply(Operation):
    def __init__(self,x,y):
        super().__init__([x,y])
        
    def compute(self,x_var,y_var):
        self.input = [x_var,y_var]
        return x_var * y_var
    
class matmul(Operation):
    def __init__(self,x,y):
        super().__init__([x,y])
        
    def compute(self,x_var,y_var):
        self.input = [x_var,y_var]
        return x_var.dot(y_var)
    

class Placeholder():
    def __init__(self):
        
        self.output_node = []
        _default_graph.placeholders.append(self)
        
class Variable():
    def __init__(self, initial_value = None):
        
        self.value = initial_value
        self.output_node= []
        
        _default_graph.variables.append(self)
        
        
class Graph():
    
    
    def __init__(self):
        
        self.operations = []
        self.placeholders = []
        self.variables = []
        
    def set_as_default(self):
        """
        Sets this Graph instance as the Global Default Graph
        """
        global _default_graph
        _default_graph = self
'''        
z = Ax + b
A = 10
b = 1

z = 10x + 1
'''
g = Graph()
g.set_as_default()
A = Variable(10)
b = Variable(1)
x = Placeholder()

y = multiply(A,x)
z = add(y,b)

#creating session
def traverse_postorder(operation):
    nodes_postorder = []
    def recurse(node):
        if isinstance(node, Operation):
            for input_node in node.input_node:
                recurse(input_node)
        nodes_postorder.append(node)
        
    recurse(operation)
    return nodes_postorder

class Session():
    def run(self, operation, feed_dict={}):
        nodes_postorder = traverse_postorder(operation)
        
        for node in nodes_postorder:
            
            if type(node) == Placeholder:
                node.output = feed_dict[node]
                
            elif type(node) == Variable:
                node.output = node.value
                
            else:
                node.inputs = [input_node.output for input_node in node.input_nodes]
                node.output = node.compute(*node.inputs)
                
            if type(node.output) == list:
                node.output = np.array(node.output)
                
        return operation.output


sess = Session()
result = sess.run(operation=z, feed_dict={x:10})
result












