#This is to use GridSearch() function to find optimal model autometically

from sklearn import svm, metrics
from sklearn.model_selection import train_test_split
from sklearn.datasets import fetch_mldata, load_digits

from sklearn.model_selection import GridSearchCV

digits = load_digits()
n_samples = len(digits.images)
data = digits.images.reshape((n_samples, -1))
X_train, X_test, y_train, y_test = train_test_split(data, digits.target, test_size=0.3, random_state=0)

classifier = svm.SVC()
classifier.fit(X_train, y_train)
predicted = classifier.predict(X_test)
print("Accuracy: %.3f" % metrics.accuracy_score(y_test, predicted))

svc = svm.SVC()

hyperparam_grid = {
    'kernel':('linear', 'rbf'),
    'gamma':[0.00001, 0.0001, 0.001, 0.01, 0.1, 1],
    'C':[1, 3, 5, 7, 9]
}

classifier = GridSearchCV(svc, hyperparam_grid)
classifier.fit(X_train, y_train)

print('Best score for data1:', classifier.best_score_)

print('Best Kernel:\t%s' % classifier.best_estimator_.kernel)
print('Best Gamma:\t%s' % classifier.best_estimator_.gamma)
print('Best C:\t\t%s' % classifier.best_estimator_.C)

predicted = classifier.predict(X_test)
print("Accuracy: %.3f" % metrics.accuracy_score(y_test, predicted))


len(data[0])

print("Classification report for classifier %s:\n%s\n"
      % (classifier, metrics.classification_report(y_test, predicted)))
print("Confusion matrix:\n%s" % metrics.confusion_matrix(y_test, predicted))